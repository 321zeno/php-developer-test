<?php

use Laravel\Lumen\Application;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\MessageBag;
use App\Http\Controllers\v1\VideoController;

class VideoControllerTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->videoRepository = $this->mock('App\Repositories\VideoRepository');
        $this->videoRepository->shouldReceive('isValid')
                        ->once()
                        ->andReturn(new MessageBag);
        $this->videoRepository->shouldReceive('save')
                        ->once()
                        ->andReturn('path');
        $this->videoRepository->shouldReceive('analyse')
                        ->once()
                        ->andReturn(['foo' => 'bar']);
    }

    public function mock($class)
    {
        $mock = Mockery::mock($class);
        $this->app->instance($class, $mock);
        return $mock;
    }


    public function testUpload()
    {
        $request = Request::create('api/v1/videos', 'post');
        $videoController = new VideoController($this->videoRepository);
        $this->assertEquals($videoController->upload($request)->getContent(), json_encode(['foo' => 'bar']));
    }
}
