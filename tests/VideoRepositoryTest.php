<?php

use App\Repositories\VideoRepository;
use Laravel\Lumen\Application;
// use getID3;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class VideoRepositoryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $app = new Application();
        $validator = $app['Illuminate\Contracts\Validation\Factory'];
        $getID3 = new getID3();
        $this->videoRepository = new VideoRepository($getID3, $validator);
        $this->testVideoName = 'test.mp4';
        $this->destinationVideoPath = base_path('public/videos/' . $this->testVideoName);
    }

    public function copyStub()
    {
        $stub = base_path('tests/stubs/' . $this->testVideoName);
        $tempPath = sys_get_temp_dir().'/'. $this->testVideoName;
        copy($stub, $tempPath);
        $this->sourceVideoPath = $tempPath;
    }

    public function testValidation()
    {
        $this->copyStub();
        $video = new UploadedFile($this->sourceVideoPath, $this->testVideoName, 'video/mp4', 100023, null, $test = true);
        $request = Request::create('api/v1/videos', 'post', [], [], ['video' => $video]);
        $this->assertTrue($this->videoRepository->isValid($request)->isEmpty());
    }

    public function testSave()
    {
        $this->copyStub();
        $video = new UploadedFile($this->sourceVideoPath, $this->testVideoName, 'video/mp4', 100023, null, $test = true);
        $request = Request::create('api/v1/videos', 'post', [], [], ['video' => $video]);
        $this->assertEquals($this->videoRepository->save($request), $this->destinationVideoPath);
    }

    public function testAnalysis()
    {
        $this->copyStub();
        $testFile = new UploadedFile($this->sourceVideoPath, $this->testVideoName, 'video/mp4', 100023, null, $test = true);
        $this->assertTrue(is_array($this->videoRepository->analyse($this->destinationVideoPath)));
    }
}
