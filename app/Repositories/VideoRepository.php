<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Factory as Validator;
use getID3;

class VideoRepository
{
    private $settings;

    public function __construct(getID3 $getID3, Validator $validator)
    {
        $this->getID3 = $getID3;
        $this->validator = $validator;
        $this->settings = [
            'request_key'     => 'video',
            'upload_location' => 'public/videos',
        ];
    }

    public function isValid(Request $request)
    {
        $validator = $this->validator->make($request->all(), [
            $this->settings['request_key'] => 'required|mimetypes:video/avi,video/h261,video/h263,video/mp4,video/mpeg,video/ogg,video/quicktime,video/webm',
        ]);
        return $validator->errors();
    }

    public function save(Request $request)
    {
        if ($request->file($this->settings['request_key'])->isValid()) {
            $path = base_path($this->settings['upload_location']);
            $file = $request->file('video');
            $name = $file->getClientOriginalName();
            $file->move($path, $name);
            return $path . '/' . $name;
        }
        return null;
    }

    public function analyse($path)
    {
        $videoInfo = $this->getID3->analyze($path);
        return static::array_utf8_encode($videoInfo);
    }

    private static function array_utf8_encode($data)
    {
        if (is_string($data)) {
            return utf8_encode($data);
        }
        if (!is_array($data)) {
            return $data;
        }
        $return = [];
        foreach ($data as $i => $d) {
            $return[$i] = static::array_utf8_encode($d);
        }
        return $return;
    }
}
