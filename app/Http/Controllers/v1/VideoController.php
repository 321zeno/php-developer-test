<?php

namespace App\Http\Controllers\v1;

use App\Repositories\VideoRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VideoController extends Controller
{

    public function __construct(VideoRepository $video)
    {
        $this->video = $video;
    }

    public function upload(Request $request)
    {
        $errors = $this->video->isValid($request);
        if (!$errors->isEmpty()) {
            return response()->json(compact('errors'), 400);
        }
        if ($path = $this->video->save($request)) {
            $properties = $this->video->analyse($path);
            return response()->json($properties);
        }
    }
}
